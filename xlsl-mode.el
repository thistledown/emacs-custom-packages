;;; xlsl-mode.el --- Major mode for editing LSL (Linden Scripting Language). -*- coding: utf-8; lexical-binding: t; -*-

;; Copyright © 2008, 2009, 2010, 2011, 2015, 2016 by Xah Lee

;; Author: Xah Lee ( http://xahlee.org/ )
;; Created: 2008-10-29
;; Package-Requires: ((emacs "24.1"))
;; Keywords: LSL, Second Life, Linden Scripting Language

;;; DESCRIPTION

;; A major mode for editing LSL (Linden Scripting Language).
;; for download location and documentation, see:
;; http://xahsl.org/sl/ls-emacs.html

;; Please pay $10. Paypal to xah@xahlee.org or use amazon.com gift to xah@xahlee.org

;;; INSTALL

;; 1. create the directory ~/.emacs.d/lisp/ if it doesn't exist
;; 2. put this file xlsl-mode.el in ~/.emacs.d/lisp/
;; 3. add the following lines in your emacs init file “.emacs”:
;; (add-to-list 'load-path "~/.emacs.d/lisp/")
;; (autoload 'xlsl-mode "xlsl-mode" "Load xlsl-mode for editing Linden Scripting Lang." t)
;; 4. restart emacs.

;; suggestion: put the following in your emacs init
 ;; (global-linum-mode 1) ; turn on line numbering

;;; DOCUMENTATION

;; Full documentation is at: http://xahsl.org/sl/ls-emacs.html

;; To see the inline documentation in emacs, call `describe-mode'. If you have not load the mode type, first type Alt+x xlsl-mode

;;; HISTORY

;; version 1.8.1, 2016-12-18 • turned on lexical-binding
;; version 1.8.0, 2016-10-25 • major rewrite of the abbrev and templates
;; version 1.7.2, 2015-03-02 • major revision. • xlsl-complete-symbol now uses ido interface. • 【Tab】 key now does completion or indent, depending on its position. • added function templates.
;; version 1.6.4, 2015-02-28 • added coloring of Firestorm viewer's lsl preprocessor keywords. And other refactoring of code.
;; version 1.6.2, 2012-04-13 • fixed a few typo (which'd cause memory leak). The typos are related to these {xlsl-keywords-regexp xlsl-type-regexp xlsl-constant-regexp xlsl-event-regexp xlsl-function-regexp}
;; version 1.6.1, 2011-05-04 • added constants OBJECT_RUNNING_SCRIPT_COUNT OBJECT_SCRIPT_MEMORY OBJECT_TOTAL_SCRIPT_COUNT
;; version 1.6.0, 2011-04-25 • Added function completion for these functions: llCastRay llClearPrimMedia llGetEnv llGetLinkNumberOfSides llGetLinkPrimitiveParams llGetPrimMediaParams llGetSPMaxMemory llGetUsedMemory llGetUsername llLinkParticleSystem llRegionSayTo llRequestUsername llScriptProfiler llSetLinkPrimitiveParamsFast llSetLinkTextureAnim llSetPrimMediaParams llTextBox.
;; version 1.5.16, 2010-12-19 • Added completion for AGENT_BY_LEGACY_NAME, AGENT_BY_USERNAME, llGetDisplayName, llRequestDisplayName.
;; version 1.5.15, 2010-01-16 • Added about 5 STATUS_* constants, e.g. STATUS_BLOCK_GRAB.
;; version 1.5.14, 2009-12-14 • Minor improvement in the help menu “LSL‣About xlsl-mode.”, and added the corresponding command xlsl-about.
;; version 1.5.13, 2009-11-15 • Added AGENT Constant.
;; version 1.5.12, 2009-10-10 • Added these constants for keyword completion: OBJECT_NAME OBJECT_DESC OBJECT_POS OBJECT_ROT OBJECT_VELOCITY OBJECT_OWNER OBJECT_GROUP OBJECT_CREATOR
;; version 1.5.11, 2009-08-27 • if emacs 23, turn on linum-mode.
;; version 1.5.10, 2009-04-28 • Added constants: DATA_BORN DATA_NAME DATA_ONLINE DATA_PAYINFO
;; version 1.5.9, 2009-04-28 • Added constants: STRING_TRIM STRING_TRIM_HEAD STRING_TRIM_TAIL
;; version 1.5.8, 2009-04-27 • Added about 13 constants starting with “AGENT_”.
;; version 1.5.7, 2009-04-26 • Modified xlsl-color-vectors-region so that if a vector component is not a number between 0 to 1, no color is applied. Added chars such as < > + - * & etc to syntax table, so that emacs knows better when lsl keyword char stops.
;; version 1.5.6, 2009-04-24 • Added the “jump” lang keyword.
;; version 1.5.5, 2009-04-11 • prettify source code formatting kinda stuff.
;; version 1.5.4, 2009-04-03 • Added: llResetLandBanList llResetLandPassList llSHA1String llDetectedTouchPos llGetRegionAgentCount .
;; version 1.5.3, 2009-04-03 • Removed these: llTakeCamera llXorBase64Strings. • Added: llDetectedTouchBinormal llDetectedTouchFace llDetectedTouchNormal llDetectedTouchST llDetectedTouchUV llGetObjectDetails llGetObjectPrimCount llGetParcelDetails llGetParcelMaxPrims llGetParcelPrimCount llGetParcelPrimOwners llRegionSay llSetClickAction llSetLinkPrimitiveParams llSetLinkTexture llStringTrim
;; version 1.5.1, 2009-04-03 • Added these keywords: llGetAgentLanguage llGetFreeURLs llGetHTTPHeader llHTTPResponse llReleaseURL llRequestSecureURL llRequestURL.
;; version 1.5, 2009-04-01 • Added a customizable variable xlsl-mode-format-style. Its value should be a integer. If 0, then no automatic formating are done. The Tab key insert a tab. If 1, then the formating style emulates that of embedded LSL client (implemented by loading c-mode). If 2, then the formatting style is whatever you have setup for c-mode.
;; version 1.4.0, 2009-03-19. • Added a user function xlsl-color-vectors-region. This will color vector text such as “<1, 0, .5>” using its own value. • Much improved xlsl-convert-rgb. Now it converts text under cursor between #rrggbb and <r, g, b> formats. • Minor fix on xlsl-complete-symbol, so that it wont give lisp error when there's no text before cursor. • Minor implementation cleanup on xlsl-lookup-lsl-ref and xlsl-lookup-lsl-ref2.
;; version 1.3.3, 2009-03-14. Fixed syntax table so that “=” is a punctuation and not “word” or “symbol”. So that, commands like forward-word etc when used on “lengthX= 3;” will stop on the “X” and not “=”.
;; version 1.3.2.1, 2009-03-04. Fix on xlsl-lookup-lsl-ref and xlsl-lookup-lsl-ref2 so that if a region contain a space char, it still works.
;; version 1.3.2, 2009-03-04. Added a second lsl wiki ref lookup: xlsl-lookup-lsl-ref2. One for looking up “lslwiki.net”, and the other for “wiki.secondlife.com”.
;; version 1.3.1, 2009-03-04. Fixed hooks by changing run-hooks to run-mode-hooks.
;; version 1.3.0, 2009-03-03. Added keyword completion feature. xlsl-complete-symbol
;; version 1.2.3, 2009-03-02. Changed the file name from “lsl-mode_Xah_Lee.el” to “xlsl-mode.el”.
;; version 1.2.2, 2009-02-24. Minior inline doc wording tweak.
;; version 1.2.1, 2009-02-20. Fixed a type in hook (run-hooks 'xlsl-mode-hook).
;; version 1.2, 2009-02-17. Added a graphical menu.
;; version 1.1.4, 2009-01-27. No code change. Minor doc change.
;; version 1.1.3, 2008-11-19. Forgot the damn (interactive) in xlsl-mode. Now fixed. Fixed also a typo where “'word” should've been “'words”, which causes parts of user variable got highlighted as keyword. e.g. the “for” in “inform”.
;; version 1.1.1, 2008-11-18. Fixed syntax table for “<”, “>” so that they are not matching pairs.
;; version 1.1, 2008-10-31. Made it call c-mode to support indentation. Added a keybinding for xlsl-convert-rgb.
;; version 1.0, 2008-10-29. First version.

;;; Code:

(require 'thingatpt )
(require 'company)

(defvar xlsl-mode-version)
(setq xlsl-mode-version "1.8.0")

(defgroup xlsl-mode nil
  "Major mode for editing Linden Scripting Language."
  :group 'languages)

(defcustom xlsl-lslint-path ""
  "Full path to lslint utility.
The value must be lslint's full path, not its parent dir.
This variable is used by the command `xlsl-syntax-check'.
lslint can be downloaded at http://w-hat.com/lslint as of 2009-03."
  :type '(file :must-match t)
  :group 'xlsl-mode)

(defcustom xlsl-mode-format-style 1
  "Specifies how source code auto formatting is done.
The value should be a integer. If 0, then no automatic formating are done. The Tab key insert a tab. If 1, then the formating style emulates that of embedded LSL client (implemented by loading c-mode). If 2, then the formatting style is whatever you have setup for c-mode."
  :type '(integer)
  :group 'xlsl-mode)

(defcustom xlsl-reference-url "http://lslwiki.net/lslwiki/wakka.php?wakka="
  "URL for LSL reference website.
The value is used by `xlsl-lookup-lsl-ref'.
The value should be one of:
“http://lslwiki.net/lslwiki/wakka.php?wakka=”
“http://wiki.secondlife.com/wiki/”"
  :type '(string)
  :group 'xlsl-mode)

(defcustom xlsl-reference-url2 "http://wiki.secondlife.com/wiki/"
  "URL for LSL reference website.
The value is used by `xlsl-lookup-lsl-ref2'.
The value can be any of:
“http://wiki.secondlife.com/wiki/”
“http://en.wikipedia.org/wiki/”
“http://www.google.com/search?q=”"
  :type '(string)
  :group 'xlsl-mode)

(defvar xlsl-mode-hook nil "Standard hook for xlsl-mode.")

(defvar xlsl-mode-map nil "Keymap for xlsl-mode")

(progn
  (setq xlsl-mode-map (make-sparse-keymap))
  (define-key xlsl-mode-map (kbd "C-c C-l") 'xlsl-syntax-check)
  (define-key xlsl-mode-map (kbd "C-c C-g") 'xlsl-convert-rgb)

  (define-key xlsl-mode-map [remap comment-dwim] 'xlsl-comment-dwim)

  (define-key xlsl-mode-map [menu-bar] (make-sparse-keymap))

  (let ((menuMap (make-sparse-keymap "LSL")))
    (define-key xlsl-mode-map [menu-bar xlsl] (cons "LSL" menuMap))

    (define-key menuMap [goto-home-page] '("Goto xlsl-mode website" . (lambda () (interactive) (browse-url "http://xahsl.org/sl/ls-emacs.html"))))

    (define-key menuMap [about] '("About xlsl-mode" . xlsl-about))

    (define-key menuMap [customize] '("Customize xlsl-mode" . (lambda () (interactive) (customize-group 'xlsl-mode))))

    (define-key menuMap [separator] '("--"))
    (define-key menuMap [convert-rgb] '("Convert #rrggbb under cursor" . xlsl-convert-rgb))
    (define-key menuMap [color-vectors] '("Color vectors in region" . xlsl-color-vectors-region))
    (define-key menuMap [copy-all] '("Copy whole buffer" . xlsl-copy-all))
    (define-key menuMap [syntax-check] '("Check syntax" . xlsl-syntax-check))
    (define-key menuMap [lookup-onlne-doc2] '("Lookup ref second site" . xlsl-lookup-lsl-ref2))
    (define-key menuMap [lookup-onlne-doc] '("Lookup ref on current word" . xlsl-lookup-lsl-ref))
    (define-key menuMap [keyword-completion] '("Indent/Completion" . xlsl-complete-or-indent))))

;;; syntax table
(defvar xlsl-mode-syntax-table
  (let ((synTable (make-syntax-table)))
    (modify-syntax-entry ?\/ ". 12b" synTable)
    (modify-syntax-entry ?\n "> b" synTable)

    (modify-syntax-entry ?= "." synTable)
    (modify-syntax-entry ?< "." synTable)
    (modify-syntax-entry ?> "." synTable)
    (modify-syntax-entry ?+ "." synTable)
    (modify-syntax-entry ?- "." synTable)
    (modify-syntax-entry ?* "." synTable)
    (modify-syntax-entry ?& "." synTable)
    (modify-syntax-entry ?| "." synTable)

    synTable)
  "Syntax table for `xlsl-mode'.")

;;; functions

(defun xlsl-about ()
  "Show the author, version number, and description about this package."
  (interactive)
  (with-output-to-temp-buffer "*About xlsl-mode*"
    (princ
     (concat "Mode name: xlsl-mode.\n\n"
             "Author: Xah Lee\n\n"
             "Version: " xlsl-mode-version "\n\n"
             "To see inline documentation, type “Alt+x `describe-mode'” while you are in xlsl-mode.\n\n"
             "Home page: URL `http://xahsl.org/sl/ls-emacs.html' \n\n"))))

(defun xlsl-copy-all ()
  "Copy buffer content into the kill-ring.
If narrow-to-region is in effect, then just copy that region."
  (interactive)
  (kill-ring-save (point-min) (point-max))
  (message "Buffer content copied."))

(defun xlsl-syntax-check ()
  "Validate LSL syntax of the current file.
You need to have lslint installed, and have `xlsl-lslint-path' set."
  (interactive)
  (if (not (= (length xlsl-lslint-path) 0))
      (shell-command (concat xlsl-lslint-path " \"" (buffer-file-name) "\""))
      (message "Error: xlsl-lslint-path not set.")))

;; implementation using “newcomment.el”.
(defun xlsl-comment-dwim (arg)
"Comment or uncomment current line or region in a smart way.
For detail, see `comment-dwim'."
   (interactive "*P")
   (require 'newcomment)
   (let ((deactivate-mark nil) (comment-start "//") (comment-end ""))
     (comment-dwim arg)))

(defun downcase-word-or-region ()
  "Downcase current word or region."
(interactive)
(let (pos1 pos2 bds)
  (if (and transient-mark-mode
           mark-active)
      (setq pos1 (region-beginning) pos2 (region-end))
    (progn
      (setq bds (bounds-of-thing-at-point 'symbol))
      (setq pos1 (car bds) pos2 (cdr bds))))

  ;; now, pos1 and pos2 are the starting and ending positions of the current word, or current text selection if exist. Sample code:
  (downcase-region pos1 pos2)
))

(defun xlsl-lookup-lsl-site (site-url)
  "Switch to browser to particular URL.
SITE-URL is a URL string.
This is a internal function.
This function is called by xlsl-lookup-lsl-ref and xlsl-lookup-lsl-ref2."
  (let (pos1 pos2 bds meat myurl)
    (if (and transient-mark-mode mark-active)
        (setq pos1 (region-beginning)
              pos2 (region-end))
      (progn
        (setq bds (bounds-of-thing-at-point 'symbol))
        (setq pos1 (car bds) pos2 (cdr bds))))

    (setq meat
          (if (and pos1 pos2 )
              (buffer-substring-no-properties pos1 pos2)
            nil ))

    (if meat
        (progn
          (setq meat (replace-regexp-in-string " " "%20" meat))
          (setq myurl (concat site-url meat))
          (browse-url myurl))
      (progn (ding) (message "No word under cursor to lookup.")))))

(defun xlsl-lookup-lsl-ref ()
  "Look up current word in LSL ref site in a browser.
If there is a text selection (a phrase), lookup that phrase.
Set variable xlsl-reference-url if you want to change the url used.

See also `xlsl-lookup-lsl-ref2'."
  (interactive)
  (xlsl-lookup-lsl-site xlsl-reference-url))

(defun xlsl-lookup-lsl-ref2 ()
  "Look up current word in LSL ref site in a browser.
If there is a text selection (a phrase), lookup that phrase.
Set variable xlsl-reference-url2 if you want to change the url used.

See also `xlsl-lookup-lsl-ref'."
  (interactive)
  (xlsl-lookup-lsl-site xlsl-reference-url2))

(defun xlsl-convert-color-hex-to-vec (hexcolor)
  "Convert HEXCOLOR from “\"rrggbb\"” format to “[r g b]” format.
Example: \"00ffcc\" ⇒ [0.0 1.0 0.8]

Note: The input string must not start with “#”.
If so, the return value is nil."
(when (= 6 (length hexcolor))
  (vector (/ (float (string-to-number (substring hexcolor 0 2) 16)) 255.0)
          (/ (float (string-to-number (substring hexcolor 2 4) 16)) 255.0)
          (/ (float (string-to-number (substring hexcolor 4) 16)) 255.0))))

(defun xlsl-convert-color-vec-to-hex (rgb)
  "Convert color RGB from “[r g b]” format to “\"rrggbb\"” format.
The RGB can be a lisp vector or list.
Example: [0 1 0.5] ⇒ \"00ff80\""
  (mapconcat
   (lambda (x)
     (format "%02x" (round (* x 255.0)) ))
   rgb ""))

(defun xlsl-color-vectors-region (start end)
  "Color all vectors <x,y,z> under region.
The color is chosen from the vector's values, where
x, y, z, are taken to be r, g, b.

This function is useful when you work a lot with colors.
It lets you visually see what color each vector represent.

See also: `xlsl-convert-rgb' and `list-colors-display'."
  (interactive "r")
  (save-restriction
    (narrow-to-region start end)
    (goto-char (point-min))
    (while (search-forward-regexp
            "< *\\([0-9.]+\\) *, *\\([0-9.]+\\) *, *\\([0-9.]+\\) *>" nil t)
      (when
          (and
           (<= (string-to-number (match-string 1)) 1.0)
           (<= (string-to-number (match-string 2)) 1.0)
           (<= (string-to-number (match-string 3)) 1.0))
  (put-text-property
   (match-beginning 0)
   (match-end 0) 'font-lock-face
   (list :background
         (concat "#" (xlsl-convert-color-vec-to-hex
                      (vector
                       (string-to-number (match-string 1))
                       (string-to-number (match-string 2))
                       (string-to-number (match-string 3))))))) ) )))

(defun xlsl-convert-rgb ()
  "Convert color spec under cursor between “#rrggbb” and “<r,g,b>”.
This command acts on texts near the cursor position.  For
example, if cursor is somewhere on the text “#ff00aa”, it becomes
“<1.0000,0.0000,0.6667>”, and vice versa.

See also `xlsl-color-vectors-region' and `list-colors-display'.
Version 2016-12-18"
  (interactive)
  (let (pos1 pos2 bds currentWord currentPos p1 p2 currentVec)
    (setq bds (bounds-of-thing-at-point 'word))
    (setq pos1 (car bds) pos2 (cdr bds))

    (if (and pos1 pos2
               (setq currentWord (buffer-substring-no-properties pos1 pos2))
               (string-match "[a-fA-F0-9]\\{6\\}" currentWord))

        ;; the case when current word is of the form #rrggbb
        (progn
          (delete-region pos1 pos2)
          (if (looking-back "#" 1)
              (delete-char -1))
          (insert "<"
                  (mapconcat
                   (lambda (x)
                     (format "%.4f" x))
                   (xlsl-convert-color-hex-to-vec currentWord)
                   ",") ">"))

        ;; the case when current word not the form #rrggbb. Try to grab a vector <...>
      (progn
        (setq currentPos (point))
        (setq p1 (search-backward "<" (- currentPos 35 ) t))
        (setq p2 (search-forward ">" (+ currentPos 35 ) t))
        (if (or (null p1) (null p2))
            (message "Failed to convert color.\nCursor does not appear to be on a color hex #rrggbb or inside a vector <r, g, b>.")
          (progn
            (setq currentVec (buffer-substring-no-properties p1 p2))
            (delete-region p1 p2)
            (insert "#" (xlsl-convert-color-vec-to-hex
                         (mapcar 'string-to-number
                                 (split-string currentVec "[<> ,]+" t))))))))))


;;; font-lock

(defvar xlsl-keywords nil "LSL keywords.")
(setq xlsl-keywords '("break" "default" "do" "else" "for" "if" "return" "state" "while" "jump")
)

(defvar xlsl-types nil "LSL types.")
(setq xlsl-types '("float" "integer" "key" "list" "rotation" "string" "vector"))

(defvar xlsl-constants nil "LSL constants.")
(setq xlsl-constants '(
"ACTIVE"
"AGENT"
"AGENT_ALWAYS_RUN"
"AGENT_ATTACHMENTS"
"AGENT_AWAY"
"AGENT_BUSY"
"AGENT_BY_LEGACY_NAME"
"AGENT_BY_USERNAME"
"AGENT_CROUCHING"
"AGENT_FLYING"
"AGENT_IN_AIR"
"AGENT_MOUSELOOK"
"AGENT_ON_OBJECT"
"AGENT_SCRIPTED"
"AGENT_SITTING"
"AGENT_TYPING"
"AGENT_WALKING"
"ALL_SIDES"
"ATTACH_BACK"
"ATTACH_CHEST"
"ATTACH_HEAD"
"ATTACH_LFOOT"
"ATTACH_LHAND"
"ATTACH_LSHOULDER"
"ATTACH_RFOOT"
"ATTACH_RHAND"
"ATTACH_RSHOULDER"
"CHANGED_ALLOWED_DROP"
"CHANGED_COLOR"
"CHANGED_INVENTORY"
"CHANGED_LINK"
"CHANGED_OWNER"
"CHANGED_REGION"
"CHANGED_SCALE"
"CHANGED_SHAPE"
"CHANGED_TELEPORT"
"CHANGED_TEXTURE"
"CONTROL_BACK"
"CONTROL_DOWN"
"CONTROL_FWD"
"CONTROL_LBUTTON"
"CONTROL_LEFT"
"CONTROL_ML_LBUTTON"
"CONTROL_RIGHT"
"CONTROL_ROT_LEFT"
"CONTROL_ROT_RIGHT"
"CONTROL_UP"
"DATA_BORN"
"DATA_NAME"
"DATA_ONLINE"
"DATA_PAYINFO"
"DEBUG_CHANNEL"
"DEG_TO_RAD"
"EOF"
"FALSE"
"HTTP_BODY_MAXLENGTH"
"HTTP_BODY_TRUNCATED"
"HTTP_METHOD"
"HTTP_MIMETYPE"
"HTTP_VERIFY_CERT"
"INVENTORY_ALL"
"INVENTORY_ANIMATION"
"INVENTORY_BODYPART"
"INVENTORY_CLOTHING"
"INVENTORY_GESTURE"
"INVENTORY_LANDMARK"
"INVENTORY_NONE"
"INVENTORY_NOTECARD"
"INVENTORY_OBJECT"
"INVENTORY_SCRIPT"
"INVENTORY_SOUND"
"INVENTORY_TEXTURE"
"LAND_LARGE_BRUSH"
"LAND_LEVEL"
"LAND_LOWER"
"LAND_MEDIUM_BRUSH"
"LAND_RAISE"
"LAND_SMALL_BRUSH"
"LINK_ALL_CHILDREN"
"LINK_ALL_OTHERS"
"LINK_ROOT"
"LINK_SET"
"LINK_THIS"
"NULL_KEY"
"OBJECT_CREATOR"
"OBJECT_DESC"
"OBJECT_GROUP"
"OBJECT_NAME"
"OBJECT_OWNER"
"OBJECT_POS"
"OBJECT_ROT"
"OBJECT_RUNNING_SCRIPT_COUNT"
"OBJECT_SCRIPT_MEMORY"
"OBJECT_TOTAL_SCRIPT_COUNT"
"OBJECT_VELOCITY"
"PARCEL_MEDIA_COMMAND_AGENT"
"PARCEL_MEDIA_COMMAND_AUTO_ALIGN"
"PARCEL_MEDIA_COMMAND_LOOP"
"PARCEL_MEDIA_COMMAND_PAUSE"
"PARCEL_MEDIA_COMMAND_PLAY"
"PARCEL_MEDIA_COMMAND_STOP"
"PARCEL_MEDIA_COMMAND_TEXTURE"
"PARCEL_MEDIA_COMMAND_TIME"
"PARCEL_MEDIA_COMMAND_UNLOAD"
"PARCEL_MEDIA_COMMAND_URL"
"PASSIVE"
"PAYMENT_INFO_ON_FILE"
"PAYMENT_INFO_USED"
"PAY_DEFAULT"
"PAY_HIDE"
"PERMISSION_ATTACH"
"PERMISSION_CHANGE_JOINTS"
"PERMISSION_CHANGE_LINKS"
"PERMISSION_CHANGE_PERMISSIONS"
"PERMISSION_CONTROL_CAMERA"
"PERMISSION_DEBIT"
"PERMISSION_RELEASE_OWNERSHIP"
"PERMISSION_REMAP_CONTROLS"
"PERMISSION_TAKE_CONTROLS"
"PERMISSION_TRACK_CAMERA"
"PERMISSION_TRIGGER_ANIMATION"
"PI"
"PI_BY_TWO"
"PRIM_BUMP_BARK"
"PRIM_BUMP_BLOBS"
"PRIM_BUMP_BRICKS"
"PRIM_BUMP_BRIGHT"
"PRIM_BUMP_CHECKER"
"PRIM_BUMP_CONCRETE"
"PRIM_BUMP_DARK"
"PRIM_BUMP_DISKS"
"PRIM_BUMP_GRAVEL"
"PRIM_BUMP_LARGETILE"
"PRIM_BUMP_NONE"
"PRIM_BUMP_SHINY"
"PRIM_BUMP_SIDING"
"PRIM_BUMP_STONE"
"PRIM_BUMP_STUCCO"
"PRIM_BUMP_SUCTION"
"PRIM_BUMP_TILE"
"PRIM_BUMP_WEAVE"
"PRIM_BUMP_WOOD"
"PRIM_COLOR"
"PRIM_GLOW"
"PRIM_HOLE_CIRCLE"
"PRIM_HOLE_DEFAULT"
"PRIM_HOLE_SQUARE"
"PRIM_HOLE_TRIANGLE"
"PRIM_MATERIAL"
"PRIM_MATERIAL_FLESH"
"PRIM_MATERIAL_GLASS"
"PRIM_MATERIAL_LIGHT"
"PRIM_MATERIAL_METAL"
"PRIM_MATERIAL_PLASTIC"
"PRIM_MATERIAL_RUBBER"
"PRIM_MATERIAL_STONE"
"PRIM_MATERIAL_WOOD"
"PRIM_PHANTOM"
"PRIM_PHYSICS"
"PRIM_POSITION"
"PRIM_ROTATION"
"PRIM_SHINY_HIGH"
"PRIM_SHINY_LOW"
"PRIM_SHINY_MEDIUM"
"PRIM_SHINY_NONE"
"PRIM_SIZE"
"PRIM_TEMP_ON_REZ"
"PRIM_TEXTURE"
"PRIM_TYPE"
"PRIM_TYPE"
"PRIM_TYPE_BOX"
"PRIM_TYPE_CYLINDER"
"PRIM_TYPE_PRISM"
"PRIM_TYPE_RING"
"PRIM_TYPE_SPHERE"
"PRIM_TYPE_TORUS"
"PRIM_TYPE_TUBE"
"PSYS_PART_BOUNCE_MASK"
"PSYS_PART_EMISSIVE_MASK"
"PSYS_PART_END_ALPHA"
"PSYS_PART_END_COLOR"
"PSYS_PART_END_SCALE"
"PSYS_PART_FLAGS"
"PSYS_PART_FOLLOW_SRC_MASK"
"PSYS_PART_FOLLOW_VELOCITY_MASK"
"PSYS_PART_INTERP_COLOR_MASK"
"PSYS_PART_INTERP_SCALE_MASK"
"PSYS_PART_MAX_AGE"
"PSYS_PART_START_ALPHA"
"PSYS_PART_START_COLOR"
"PSYS_PART_START_SCALE"
"PSYS_PART_TARGET_LINEAR_MASK"
"PSYS_PART_TARGET_POS_MASK"
"PSYS_PART_WIND_MASK"
"PSYS_SRC_ACCEL"
"PSYS_SRC_ANGLE_BEGIN"
"PSYS_SRC_ANGLE_END"
"PSYS_SRC_BURST_PART_COUNT"
"PSYS_SRC_BURST_RADIUS"
"PSYS_SRC_BURST_RATE"
"PSYS_SRC_BURST_SPEED_MAX"
"PSYS_SRC_BURST_SPEED_MIN"
"PSYS_SRC_MAX_AGE"
"PSYS_SRC_OMEGA"
"PSYS_SRC_PATTERN"
"PSYS_SRC_PATTERN_ANGLE"
"PSYS_SRC_PATTERN_ANGLE_CONE"
"PSYS_SRC_PATTERN_ANGLE_CONE_EMPTY"
"PSYS_SRC_PATTERN_DROP"
"PSYS_SRC_PATTERN_EXPLODE"
"PSYS_SRC_TARGET_KEY"
"PSYS_SRC_TEXTURE"
"PUBLIC_CHANNEL"
"RAD_TO_DEG"
"REMOTE_DATA_CHANNEL"
"REMOTE_DATA_REPLY"
"REMOTE_DATA_REQUEST"
"SCRIPTED"
"SQRT2"
"STATUS_BLOCK_GRAB"
"STATUS_CAST_SHADOWS"
"STATUS_DIE_AT_EDGE"
"STATUS_PHANTOM"
"STATUS_PHYSICS"
"STATUS_RETURN_AT_EDGE"
"STATUS_ROTATE_X"
"STATUS_ROTATE_Y"
"STATUS_ROTATE_Z"
"STATUS_SANDBOX"
"STRING_TRIM"
"STRING_TRIM_HEAD"
"STRING_TRIM_TAIL"
"TRUE"
"TWO_PI"
"TYPE_FLOAT"
"TYPE_INTEGER"
"TYPE_INVALID"
"TYPE_KEY"
"TYPE_QUATERNION"
"TYPE_STRING"
"TYPE_VECTOR"
"ZERO_ROTATION"
"ZERO_VECTOR"
))

(defvar xlsl-events nil "LSL events.")
(setq xlsl-events '("at_rot_target" "at_target" "attach" "changed" "collision" "collision_end" "collision_start" "control" "dataserver" "email" "http_response" "land_collision" "land_collision_end" "land_collision_start" "link_message" "listen" "money" "moving_end" "moving_start" "no_sensor" "not_at_rot_target" "not_at_target" "object_rez" "on_rez" "remote_data" "run_time_permissions" "sensor" "state_entry" "state_exit" "timer" "touch" "touch_end" "touch_start"))

(defvar xlsl-functions nil "LSL functions.")
(setq xlsl-functions '(
"llAbs"
"llAcos"
"llAddToLandBanList"
"llAddToLandPassList"
"llAdjustSoundVolume"
"llAllowInventoryDrop"
"llAngleBetween"
"llApplyImpulse"
"llApplyRotationalImpulse"
"llAsin"
"llAtan2"
"llAttachToAvatar"
"llAvatarOnSitTarget"
"llAxes2Rot"
"llAxisAngle2Rot"
"llBase64ToInteger"
"llBase64ToString"
"llBreakAllLinks"
"llBreakLink"
"llCSV2List"
"llCastRay"
"llCeil"
"llClearCameraParams"
"llClearPrimMedia"
"llCloseRemoteDataChannel"
"llCloud"
"llCollisionFilter"
"llCollisionSound"
"llCollisionSprite"
"llCos"
"llCreateLink"
"llDeleteSubList"
"llDeleteSubString"
"llDetachFromAvatar"
"llDetectedGrab"
"llDetectedGroup"
"llDetectedKey"
"llDetectedLinkNumber"
"llDetectedName"
"llDetectedOwner"
"llDetectedPos"
"llDetectedRot"
"llDetectedTouchBinormal"
"llDetectedTouchFace"
"llDetectedTouchNormal"
"llDetectedTouchPos"
"llDetectedTouchST"
"llDetectedTouchUV"
"llDetectedType"
"llDetectedVel"
"llDialog"
"llDie"
"llDumpList2String"
"llEdgeOfWorld"
"llEjectFromLand"
"llEmail"
"llEscapeURL"
"llEuler2Rot"
"llFabs"
"llFloor"
"llForceMouselook"
"llFrand"
"llGetAccel"
"llGetAgentInfo"
"llGetAgentLanguage"
"llGetAgentSize"
"llGetAlpha"
"llGetAndResetTime"
"llGetAnimation"
"llGetAnimationList"
"llGetAttached"
"llGetBoundingBox"
"llGetCameraPos"
"llGetCameraRot"
"llGetCenterOfMass"
"llGetColor"
"llGetCreator"
"llGetDate"
"llGetDisplayName"
"llGetEnergy"
"llGetEnv"
"llGetForce"
"llGetFreeMemory"
"llGetFreeURLs"
"llGetGMTclock"
"llGetGeometricCenter"
"llGetHTTPHeader"
"llGetInventoryCreator"
"llGetInventoryKey"
"llGetInventoryName"
"llGetInventoryNumber"
"llGetInventoryPermMask"
"llGetInventoryType"
"llGetKey"
"llGetLandOwnerAt"
"llGetLinkKey"
"llGetLinkName"
"llGetLinkNumber"
"llGetLinkNumberOfSides"
"llGetLinkPrimitiveParams"
"llGetListEntryType"
"llGetListLength"
"llGetLocalPos"
"llGetLocalRot"
"llGetMass"
"llGetNextEmail"
"llGetNotecardLine"
"llGetNumberOfNotecardLines"
"llGetNumberOfPrims"
"llGetNumberOfSides"
"llGetObjectDesc"
"llGetObjectDetails"
"llGetObjectMass"
"llGetObjectName"
"llGetObjectPermMask"
"llGetObjectPrimCount"
"llGetOmega"
"llGetOwner"
"llGetOwnerKey"
"llGetParcelDetails"
"llGetParcelFlags"
"llGetParcelMaxPrims"
"llGetParcelPrimCount"
"llGetParcelPrimOwners"
"llGetPermissions"
"llGetPermissionsKey"
"llGetPos"
"llGetPrimMediaParams"
"llGetPrimitiveParams"
"llGetRegionAgentCount"
"llGetRegionCorner"
"llGetRegionFPS"
"llGetRegionFlags"
"llGetRegionName"
"llGetRegionTimeDilation"
"llGetRootPosition"
"llGetRootRotation"
"llGetRot"
"llGetSPMaxMemory"
"llGetScale"
"llGetScriptName"
"llGetScriptState"
"llGetSimulatorHostname"
"llGetStartParameter"
"llGetStatus"
"llGetSubString"
"llGetSunDirection"
"llGetTexture"
"llGetTextureOffset"
"llGetTextureRot"
"llGetTextureScale"
"llGetTime"
"llGetTimeOfDay"
"llGetTimestamp"
"llGetTorque"
"llGetUnixTime"
"llGetUsedMemory"
"llGetUsername"
"llGetVel"
"llGetWallclock"
"llGiveInventory"
"llGiveInventoryList"
"llGiveMoney"
"llGround"
"llGroundContour"
"llGroundNormal"
"llGroundRepel"
"llGroundSlope"
"llHTTPRequest"
"llHTTPResponse"
"llInsertString"
"llInstantMessage"
"llIntegerToBase64"
"llKey2Name"
"llLinkParticleSystem"
"llLinks"
"llList2CSV"
"llList2Float"
"llList2Integer"
"llList2Key"
"llList2List"
"llList2ListStrided"
"llList2Rot"
"llList2String"
"llList2Vector"
"llListFindList"
"llListInsertList"
"llListRandomize"
"llListReplaceList"
"llListSort"
"llListStatistics"
"llListen"
"llListenControl"
"llListenRemove"
"llLoadURL"
"llLog"
"llLog10"
"llLookAt"
"llLoopSound"
"llLoopSoundMaster"
"llLoopSoundSlave"
"llMD5String"
"llMapDestination"
"llMessageLinked"
"llMinEventDelay"
"llModPow"
"llModifyLand"
"llMoveToTarget"
"llOffsetTexture"
"llOpenRemoteDataChannel"
"llOverMyLand"
"llOwnerSay"
"llParcelMediaCommandList"
"llParcelMediaQuery"
"llParseString2List"
"llParseStringKeepNulls"
"llParticleSystem"
"llPassCollisions"
"llPassTouches"
"llPlaySound"
"llPlaySoundSlave"
"llPointAt"
"llPow"
"llPreloadSound"
"llPushObject"
"llRefreshPrimURL"
"llRegionSay"
"llRegionSay"
"llRegionSayTo"
"llReleaseCamera"
"llReleaseControls"
"llReleaseURL"
"llRemoteDataReply"
"llRemoteDataSetRegion"
"llRemoteLoadScriptPin"
"llRemoveFromLandBanList"
"llRemoveFromLandPassList"
"llRemoveInventory"
"llRemoveVehicleFlags"
"llRequestAgentData"
"llRequestDisplayName"
"llRequestInventoryData"
"llRequestPermissions"
"llRequestSecureURL"
"llRequestSimulatorData"
"llRequestURL"
"llRequestUsername"
"llResetLandBanList"
"llResetLandPassList"
"llResetOtherScript"
"llResetScript"
"llResetTime"
"llRezAtRoot"
"llRezObject"
"llRot2Angle"
"llRot2Axis"
"llRot2Euler"
"llRot2Fwd"
"llRot2Left"
"llRot2Up"
"llRotBetween"
"llRotLookAt"
"llRotTarget"
"llRotTargetRemove"
"llRotateTexture"
"llRound"
"llSHA1String"
"llSameGroup"
"llSay"
"llScaleTexture"
"llScriptDanger"
"llScriptProfiler"
"llSendRemoteData"
"llSensor"
"llSensorRemove"
"llSensorRepeat"
"llSetAlpha"
"llSetBuoyancy"
"llSetCameraAtOffset"
"llSetCameraEyeOffset"
"llSetCameraParams"
"llSetClickAction"
"llSetColor"
"llSetDamage"
"llSetForce"
"llSetForceAndTorque"
"llSetHoverHeight"
"llSetLinkAlpha"
"llSetLinkColor"
"llSetLinkPrimitiveParams"
"llSetLinkPrimitiveParamsFast"
"llSetLinkTexture"
"llSetLinkTextureAnim"
"llSetLocalRot"
"llSetObjectDesc"
"llSetObjectName"
"llSetParcelMusicURL"
"llSetPayPrice"
"llSetPos"
"llSetPrimMediaParams"
"llSetPrimURL"
"llSetPrimitiveParams"
"llSetRemoteScriptAccessPin"
"llSetRot"
"llSetScale"
"llSetScriptState"
"llSetSitText"
"llSetSoundQueueing"
"llSetSoundRadius"
"llSetStatus"
"llSetText"
"llSetTexture"
"llSetTextureAnim"
"llSetTimerEvent"
"llSetTorque"
"llSetTouchText"
"llSetVehicleFlags"
"llSetVehicleFloatParam"
"llSetVehicleRotationParam"
"llSetVehicleType"
"llSetVehicleVectorParam"
"llShout"
"llSin"
"llSitTarget"
"llSleep"
"llSqrt"
"llStartAnimation"
"llStopAnimation"
"llStopHover"
"llStopLookAt"
"llStopMoveToTarget"
"llStopPointAt"
"llStopSound"
"llStringLength"
"llStringToBase64"
"llStringTrim"
"llSubStringIndex"
"llTakeControls"
"llTan"
"llTarget"
"llTargetOmega"
"llTargetRemove"
"llTeleportAgentHome"
"llTextBox"
"llToLower"
"llToUpper"
"llTriggerSound"
"llTriggerSoundLimited"
"llUnSit"
"llUnescapeURL"
"llVecDist"
"llVecMag"
"llVecNorm"
"llVolumeDetect"
"llWater"
"llWhisper"
"llWind"
"llXorBase64StringsCorrect"
))

(defvar xlsl-preprocessor-keywords nil "LSL preprocessor keywords, supported by Firestorm viewer.")
(setq xlsl-preprocessor-keywords '("#define" "#if" "#else" "#endif" "#include"))

(defvar xlsl-all-keywords nil "list of all LSL keywords")
(setq xlsl-all-keywords (append
xlsl-keywords
xlsl-types
xlsl-constants
xlsl-events
xlsl-functions
xlsl-preprocessor-keywords))

(setq xlsl-font-lock-keywords
      (let (
            (ξkeywords-regexp (regexp-opt xlsl-keywords 'words))
            (ξtype-regexp (regexp-opt xlsl-types 'words))
            (ξconstant-regexp (regexp-opt xlsl-constants 'words))
            (ξevent-regexp (regexp-opt xlsl-events 'words))
            (ξfunction-regexp (regexp-opt xlsl-functions 'words))
            (ξpreprocessor-regexp (regexp-opt xlsl-preprocessor-keywords ))
)
        `(
          (,ξpreprocessor-regexp . font-lock-preprocessor-face)
          (,ξtype-regexp . font-lock-type-face)
          (,ξconstant-regexp . font-lock-constant-face)
          (,ξevent-regexp . font-lock-reference-face)
          (,ξfunction-regexp . font-lock-function-name-face)
          (,ξkeywords-regexp . font-lock-keyword-face)

          ;; font-lock-builtin-face
          ;; font-lock-comment-delimiter-face
          ;; font-lock-comment-face
          ;; font-lock-constant-face
          ;; font-lock-doc-face
          ;; font-lock-function-name-face
          ;; font-lock-keyword-face
          ;; font-lock-negation-char-face
          ;; font-lock-preprocessor-face
          ;; font-lock-reference-face
          ;; font-lock-string-face
          ;; font-lock-type-face
          ;; font-lock-variable-name-face
          ;; font-lock-warning-face

          ;; note: order above matters. Keywords goes last here because, for example, otherwise the keyword “state” in the function “state_entry” would be highlighted.

          )))


;; indent

(defun xlsl-complete-or-indent ()
  "Do keyword completion or indent/prettify-format.

If char before point is letters and char after point is whitespace or punctuation, then do completion, except when in string or comment. Else, do indent.

Version 2016-12-18"
  (interactive)
  ;; consider the char to the left or right of cursor. Each side is either empty or char.
  ;; there are 4 cases:
  ;; space▮space → do indent
  ;; space▮char → do indent
  ;; char▮space → do completion
  ;; char ▮char → do indent
  (let ( (ξsyntax-state (syntax-ppss)))
    (if (or (nth 3 ξsyntax-state) (nth 4 ξsyntax-state))
        (c-indent-line-or-region)
      (if
          (and (looking-back "[-_a-zA-Z]" 99)
               (or (eobp) (looking-at "[\n[:blank:][:punct:]]")))
          (xlsl-complete-symbol)
        (c-indent-line-or-region)))))


;; completion

(defvar xlsl-kwdList nil "LSL keywords.")

(setq xlsl-kwdList (make-hash-table :test 'equal))
(mapc (lambda (x) (puthash x t xlsl-kwdList)) xlsl-keywords)
(mapc (lambda (x) (puthash x t xlsl-kwdList)) xlsl-types)
(mapc (lambda (x) (puthash x t xlsl-kwdList)) xlsl-constants)
(mapc (lambda (x) (puthash x t xlsl-kwdList)) xlsl-events)
(mapc (lambda (x) (puthash x t xlsl-kwdList)) xlsl-functions)
(put 'xlsl-kwdList 'risky-local-variable t)

(defun xlsl-complete-symbol ()
  "Perform keyword completion on current word.
This uses `ido-mode' user interface for completion."
  (interactive)
  (let* (
         (ξbds (bounds-of-thing-at-point 'symbol))
         (ξp1 (car ξbds))
         (ξp2 (cdr ξbds))
         (ξcurrent-sym
          (if  (or (null ξp1) (null ξp2) (equal ξp1 ξp2))
              ""
            (buffer-substring-no-properties ξp1 ξp2)))
         ξresult-sym)
    (when (not ξcurrent-sym) (setq ξcurrent-sym ""))
    (setq ξresult-sym
          (ido-completing-read "" xlsl-all-keywords nil nil ξcurrent-sym ))
    (delete-region ξp1 ξp2)
    (insert ξresult-sym)))

(defun xlsl-complete-symbol-2 ()
  "Perform keyword completion on word before cursor.
Uses classic emacs completion interface.
See also `xlsl-complete-symbol'"
  (interactive)
  (let ((posEnd (point))
        (meat (thing-at-point 'symbol))
        maxMatchResult)

    (when (not meat) (setq meat ""))

    (setq maxMatchResult (try-completion meat xlsl-kwdList))
    (cond ((eq maxMatchResult t))
          ((null maxMatchResult)
           (message "Can't find completion for “%s”" meat)
           (ding))
          ((not (string= meat maxMatchResult))
           (delete-region (- posEnd (length meat)) posEnd)
           (insert maxMatchResult))
          (t (message "Making completion list...")
             (with-output-to-temp-buffer "*Completions*"
               (display-completion-list
                (all-completions meat xlsl-kwdList)))
             (message "Making completion list...%s" "done")))))


;; abbrev, function template

(defun xlsl-abbrev-enable-function ()
  "Return t if not in string or comment. Else nil.
This is for abbrev table property `:enable-function'.
Version 2016-10-24"
  (let ((-syntax-state (syntax-ppss)))
    (not (or (nth 3 -syntax-state) (nth 4 -syntax-state)))))

(defun xlsl-expand-abbrev ()
  "Expand the symbol before cursor,
if cursor is not in string or comment.
Returns the abbrev symbol if there's a expansion, else nil.
Version 2016-10-24"
  (interactive)
  (when (xlsl-abbrev-enable-function) ; abbrev property :enable-function doesn't seem to work, so check here instead
    (let (
          -p1 -p2
          -abrStr
          -abrSymbol
          )
      (save-excursion
        (forward-symbol -1)
        (setq -p1 (point))
        (forward-symbol 1)
        (setq -p2 (point)))
      (setq -abrStr (buffer-substring-no-properties -p1 -p2))
      (setq -abrSymbol (abbrev-symbol -abrStr))
      (if -abrSymbol
          (progn
            (abbrev-insert -abrSymbol -abrStr -p1 -p2 )
            (xlsl--abbrev-position-cursor -p1)
            -abrSymbol)
        nil))))

(defun xlsl--abbrev-position-cursor (&optional *pos)
  "Move cursor back to ▮ if exist, else put at end.
Return true if found, else false.
Version 2016-10-24"
  (interactive)
  (message "pos is %s" *pos)
  (let ((-found-p (search-backward "▮" (if *pos *pos (max (point-min) (- (point) 100))) t )))
    (when -found-p (forward-char ))
    -found-p
    ))

(defun xlsl--ahf ()
  "Abbrev hook function, used for `define-abbrev'.
 Our use is to prevent inserting the char that triggered expansion. Experimental.
 the “ahf” stand for abbrev hook function.
Version 2016-10-24"
  t)

(put 'xlsl--ahf 'no-self-insert t)

(setq xlsl-abbrev-table nil)

(define-abbrev-table 'xlsl-abbrev-table
  '(

    ("at_rot_target" "at_rot_target(integer tnum, rotation targetrot, rotation ourrot)
{
▮
}" xlsl--ahf)
    ("attach" "attach(key id)
{
▮
}" xlsl--ahf)
    ("at_target" "at_target(integer tnum, vector targetpos, vector ourpos)
{
▮
}" xlsl--ahf)
    ("changed" "changed(integer change)
{
▮
}" xlsl--ahf)
    ("collision_end" "collision_end(integer num_detected)
{
▮
}" xlsl--ahf)
    ("collision_start" "collision_start(integer num_detected)
{
▮
}" xlsl--ahf)
    ("collision" "collision(integer num_detected)
{
▮
}" xlsl--ahf)
    ("control" "control(key id, integer held, integer change)
{
▮
}" xlsl--ahf)
    ("dataserver" "dataserver(key queryid, string data)
{
▮
}" xlsl--ahf)
    ("default" "default
{
▮
    state_entry()
    {
        llSay(0, \"Hello, Avatar!\");
    }\n
    touch_start(integer num_detected)
    {
        llSay(0, \"Touched.\");
    }
}" xlsl--ahf)
    ("do" "do
{
▮;
} while (TRUE);
" xlsl--ahf)
    ("elseif" "else if (cc == dd)
{
TRUE;
}" xlsl--ahf)
    ("else" "else
{
TRUE;
}" xlsl--ahf)
    ("email" "email(string time, string address, string subj, string message, integer num_left)
{
▮
}" xlsl--ahf)
    ("for" "integer i;
for( i = 0; i < num_detected; i++)
{
▮;
}" xlsl--ahf)
    ("http_response" "http_response(key request_id, integer status, list metadata, string body)
{
▮
}" xlsl--ahf)
    ("if" "if (aa == bb)
{
TRUE;
}" xlsl--ahf)
    ("jump" "jump loopExit
@loopExit" xlsl--ahf)
    ("land_collision_end" "land_collision_end(vector pos)
{
▮
}" xlsl--ahf)
    ("land_collision_start" "land_collision_start(vector pos)
{
▮
}" xlsl--ahf)
    ("land_collision" "land_collision(vector pos)
{
▮
}" xlsl--ahf)
    ("link_message" "link_message(integer sender_num, integer num, string str, key id)
{
▮
}" xlsl--ahf)
    ("listen" "listen(integer channel, string name, key id, string message)
{
▮
}" xlsl--ahf)
    ("llAbs" "llAbs(integer val)" xlsl--ahf)
    ("llAcos" "llAcos(float val)" xlsl--ahf)
    ("llAddToLandBanList" "llAddToLandBanList(key agent, float hours)" xlsl--ahf)
    ("llAddToLandPassList" "llAddToLandPassList(key agent, float hours)" xlsl--ahf)
    ("llAdjustSoundVolume" "llAdjustSoundVolume(float volume)" xlsl--ahf)
    ("llAllowInventoryDrop" "llAllowInventoryDrop(integer add)" xlsl--ahf)
    ("llAngleBetween" "llAngleBetween(rotation a, rotation b)" xlsl--ahf)
    ("llApplyImpulse" "llApplyImpulse(vector force, integer local)
" xlsl--ahf)
    ("llApplyRotationalImpulse" "llApplyRotationalImpulse(vector force, integer local)" xlsl--ahf)
    ("llAsin" "llAsin(float val)" xlsl--ahf)
    ("llAtan2" "llAtan2(float y, float x)" xlsl--ahf)
    ("llAttachToAvatar" "llAttachToAvatar(integer attachment)" xlsl--ahf)
    ("llAvatarOnSitTarget" "llAvatarOnSitTarget()" xlsl--ahf)
    ("llAxes2Rot" "llAxes2Rot(vector fwd, vector left, vector up)" xlsl--ahf)
    ("llAxisAngle2Rot" "llAxisAngle2Rot(vector axis, float angle)" xlsl--ahf)
    ("llBase64ToInteger" "llBase64ToInteger(string str)" xlsl--ahf)
    ("llBase64ToString" "llBase64ToString(string str)" xlsl--ahf)
    ("llBreakAllLinks" "llBreakAllLinks()" xlsl--ahf)
    ("llBreakLink" "llBreakLink(integer linknum)" xlsl--ahf)
    ("llCastRay" "llCastRay( vector start, vector end, list options )" xlsl--ahf)
    ("llCeil" "llCeil(float val)" xlsl--ahf)
    ("llClearCameraParams" "llClearCameraParams()" xlsl--ahf)
    ("llClearPrimMedia" "llClearPrimMedia( integer face )" xlsl--ahf)
    ("llCloseRemoteDataChannel" "llCloseRemoteDataChannel(key channel)" xlsl--ahf)
    ("llCloud" "llCloud(vector offset)" xlsl--ahf)
    ("llCollisionFilter" "llCollisionFilter(string name, key id, integer accept)" xlsl--ahf)
    ("llCollisionSound" "llCollisionSound(string impact_sound, float impact_volume)" xlsl--ahf)
    ("llCollisionSprite" "llCollisionSprite(string impact_sprite)" xlsl--ahf)
    ("llCos" "llCos(float theta)" xlsl--ahf)
    ("llCreateLink" "llCreateLink(key target, integer parent)" xlsl--ahf)
    ("llCSV2List" "llCSV2List(string src)" xlsl--ahf)
    ("llDeleteSubList" "llDeleteSubList(list src, integer start, integer end)" xlsl--ahf)
    ("llDeleteSubString" "llDeleteSubString(string src, integer start, integer end)" xlsl--ahf)
    ("llDetachFromAvatar" "llDetachFromAvatar()" xlsl--ahf)
    ("llDetectedGrab" "llDetectedGrab(integer number)" xlsl--ahf)
    ("llDetectedGroup" "llDetectedGroup(integer number)" xlsl--ahf)
    ("llDetectedKey" "llDetectedKey(integer number)" xlsl--ahf)
    ("llDetectedLinkNumber" "llDetectedLinkNumber(integer number)" xlsl--ahf)
    ("llDetectedName" "llDetectedName(integer number)" xlsl--ahf)
    ("llDetectedOwner" "llDetectedOwner(integer number)" xlsl--ahf)
    ("llDetectedPos" "llDetectedPos(integer number)" xlsl--ahf)
    ("llDetectedRot" "llDetectedRot(integer number)" xlsl--ahf)
    ("llDetectedTouchBinormal" "llDetectedTouchBinormal(integer index)" xlsl--ahf)
    ("llDetectedTouchFace" "llDetectedTouchFace(integer index)" xlsl--ahf)
    ("llDetectedTouchNormal" "llDetectedTouchNormal(integer index)" xlsl--ahf)
    ("llDetectedTouchPos" "llDetectedTouchPos(integer index)" xlsl--ahf)
    ("llDetectedTouchST" "llDetectedTouchST(integer index)" xlsl--ahf)
    ("llDetectedTouchUV" "llDetectedTouchUV(integer index)" xlsl--ahf)
    ("llDetectedType" "llDetectedType(integer number)" xlsl--ahf)
    ("llDetectedVel" "llDetectedVel(integer number)" xlsl--ahf)
    ("llDialog" "llDialog(key id, string message, list buttons, integer chat_channel)" xlsl--ahf)
    ("llDie" "llDie()" xlsl--ahf)
    ("llDumpList2String" "llDumpList2String(list src, string separator)" xlsl--ahf)
    ("llEdgeOfWorld" "llEdgeOfWorld(vector pos, vector dir)" xlsl--ahf)
    ("llEjectFromLand" "llEjectFromLand(key user)" xlsl--ahf)
    ("llEmail" "llEmail(string address, string subject, string message)" xlsl--ahf)
    ("llEscapeURL" "llEscapeURL(string url)" xlsl--ahf)
    ("llEuler2Rot" "llEuler2Rot(vector vec)" xlsl--ahf)
    ("llFabs" "llFabs(float num)" xlsl--ahf)
    ("llFloor" "llFloor(val)" xlsl--ahf)
    ("llForceMouselook" "llForceMouselook(integer mouselook)" xlsl--ahf)
    ("llFrand" "llFrand(float max)" xlsl--ahf)
    ("llGetAccel" "llGetAccel()" xlsl--ahf)
    ("llGetAgentInfo" "llGetAgentInfo(key id)" xlsl--ahf)
    ("llGetAgentLanguage" "llGetAgentLanguage(key id)" xlsl--ahf)
    ("llGetAgentSize" "llGetAgentSize(key id)" xlsl--ahf)
    ("llGetAlpha" "llGetAlpha(integer face)" xlsl--ahf)
    ("llGetAndResetTime" "llGetAndResetTime()" xlsl--ahf)
    ("llGetAnimationList" "llGetAnimationList(key id)" xlsl--ahf)
    ("llGetAnimation" "llGetAnimation(key id)" xlsl--ahf)
    ("llGetAttached" "llGetAttached()" xlsl--ahf)
    ("llGetBoundingBox" "llGetBoundingBox(key object)" xlsl--ahf)
    ("llGetCameraPos" "llGetCameraPos()" xlsl--ahf)
    ("llGetCameraRot" "llGetCameraRot()" xlsl--ahf)
    ("llGetCenterOfMass" "llGetCenterOfMass()" xlsl--ahf)
    ("llGetColor" "llGetColor(integer face)" xlsl--ahf)
    ("llGetCreator" "llGetCreator()" xlsl--ahf)
    ("llGetDate" "llGetDate()" xlsl--ahf)
    ("llGetDisplayName" "llGetDisplayName(key id)" xlsl--ahf)
    ("llGetEnergy" "llGetEnergy()" xlsl--ahf)
    ("llGetEnv" "llGetEnv( string name )" xlsl--ahf)
    ("llGetForce" "llGetForce()" xlsl--ahf)
    ("llGetFreeMemory" "llGetFreeMemory()" xlsl--ahf)
    ("llGetFreeURLs" "llGetFreeURLs()" xlsl--ahf)
    ("llGetGeometricCenter" "llGetGeometricCenter()" xlsl--ahf)
    ("llGetGMTclock" "llGetGMTclock()" xlsl--ahf)
    ("llGetHTTPHeader" "llGetHTTPHeader(key request_id, string header)" xlsl--ahf)
    ("llGetInventoryCreator" "llGetInventoryCreator(string item)" xlsl--ahf)
    ("llGetInventoryKey" "llGetInventoryKey(string name)" xlsl--ahf)
    ("llGetInventoryName" "llGetInventoryName(integer type, integer number)" xlsl--ahf)
    ("llGetInventoryNumber" "llGetInventoryNumber(integer type)" xlsl--ahf)
    ("llGetInventoryPermMask" "llGetInventoryPermMask(string item, integer mask)" xlsl--ahf)
    ("llGetInventoryType" "llGetInventoryType(string name)" xlsl--ahf)
    ("llGetKey" "llGetKey()" xlsl--ahf)
    ("llGetLandOwnerAt" "llGetLandOwnerAt(vector pos)" xlsl--ahf)
    ("llGetLinkKey" "llGetLinkKey(integer linknum)" xlsl--ahf)
    ("llGetLinkName" "llGetLinkName(integer linknum)" xlsl--ahf)
    ("llGetLinkNumberOfSides" "llGetLinkNumberOfSides( integer link )" xlsl--ahf)
    ("llGetLinkNumber" "llGetLinkNumber()" xlsl--ahf)
    ("llGetLinkPrimitiveParams" "llGetLinkPrimitiveParams( integer link, list params )" xlsl--ahf)
    ("llGetListEntryType" "llGetListEntryType(list src, integer index)" xlsl--ahf)
    ("llGetListLength" "llGetListLength(list src)" xlsl--ahf)
    ("llGetLocalPos" "llGetLocalPos()" xlsl--ahf)
    ("llGetLocalRot" "llGetLocalRot()" xlsl--ahf)
    ("llGetMass" "llGetMass()" xlsl--ahf)
    ("llGetNextEmail" "llGetNextEmail(string address, string subject)" xlsl--ahf)
    ("llGetNotecardLine" "llGetNotecardLine(string name, integer line)" xlsl--ahf)
    ("llGetNumberOfNotecardLines" "llGetNumberOfNotecardLines(string name)" xlsl--ahf)
    ("llGetNumberOfPrims" "llGetNumberOfPrims()" xlsl--ahf)
    ("llGetNumberOfSides" "llGetNumberOfSides()" xlsl--ahf)
    ("llGetObjectDesc" "llGetObjectDesc()" xlsl--ahf)
    ("llGetObjectDetails" "llGetObjectDetails(key id, list params)" xlsl--ahf)
    ("llGetObjectMass" "llGetObjectMass(key id)" xlsl--ahf)
    ("llGetObjectName" "llGetObjectName()" xlsl--ahf)
    ("llGetObjectPermMask" "llGetObjectPermMask(integer mask)" xlsl--ahf)
    ("llGetObjectPrimCount" "llGetObjectPrimCount(key prim)" xlsl--ahf)
    ("llGetOmega" "llGetOmega()" xlsl--ahf)
    ("llGetOwnerKey" "llGetOwnerKey(key id)" xlsl--ahf)
    ("llGetOwner" "llGetOwner()" xlsl--ahf)
    ("llGetParcelDetails" "llGetParcelDetails(vector pos, list params)" xlsl--ahf)
    ("llGetParcelFlags" "llGetParcelFlags(vector pos)" xlsl--ahf)
    ("llGetParcelMaxPrims" "llGetParcelMaxPrims(vector pos, integer sim_wide)" xlsl--ahf)
    ("llGetParcelPrimCount" "llGetParcelPrimCount(vector pos, integer category, integer sim_wide)" xlsl--ahf)
    ("llGetParcelPrimOwners" "llGetParcelPrimOwners(vector pos)" xlsl--ahf)
    ("llGetPermissionsKey" "llGetPermissionsKey()" xlsl--ahf)
    ("llGetPermissions" "llGetPermissions()" xlsl--ahf)
    ("llGetPos" "llGetPos()" xlsl--ahf)
    ("llGetPrimitiveParams" "llGetPrimitiveParams(list params)" xlsl--ahf)
    ("llGetPrimMediaParams" "llGetPrimMediaParams( integer face, list params )" xlsl--ahf)
    ("llGetRegionAgentCount" "llGetRegionAgentCount()" xlsl--ahf)
    ("llGetRegionCorner" "llGetRegionCorner()" xlsl--ahf)
    ("llGetRegionFlags" "llGetRegionFlags()" xlsl--ahf)
    ("llGetRegionFPS" "llGetRegionFPS()" xlsl--ahf)
    ("llGetRegionName" "llGetRegionName()" xlsl--ahf)
    ("llGetRegionTimeDilation" "llGetRegionTimeDilation()" xlsl--ahf)
    ("llGetRootPosition" "llGetRootPosition()" xlsl--ahf)
    ("llGetRootRotation" "llGetRootRotation()" xlsl--ahf)
    ("llGetRot" "llGetRot()" xlsl--ahf)
    ("llGetScale" "llGetScale()" xlsl--ahf)
    ("llGetScriptName" "llGetScriptName()" xlsl--ahf)
    ("llGetScriptState" "llGetScriptState(string name)" xlsl--ahf)
    ("llGetSimulatorHostname" "llGetSimulatorHostname()" xlsl--ahf)
    ("llGetSPMaxMemory" "LlGetSPMaxMemory()" xlsl--ahf)
    ("llGetStartParameter" "llGetStartParameter()" xlsl--ahf)
    ("llGetStatus" "llGetStatus(integer status)" xlsl--ahf)
    ("llGetSubString" "llGetSubString(string src, integer start, integer end)" xlsl--ahf)
    ("llGetSunDirection" "llGetSunDirection()" xlsl--ahf)
    ("llGetTextureOffset" "llGetTextureOffset(integer side)" xlsl--ahf)
    ("llGetTextureRot" "llGetTextureRot(integer side)" xlsl--ahf)
    ("llGetTextureScale" "llGetTextureScale(integer side)" xlsl--ahf)
    ("llGetTexture" "llGetTexture(integer side)" xlsl--ahf)
    ("llGetTimeOfDay" "llGetTimeOfDay()" xlsl--ahf)
    ("llGetTimestamp" "llGetTimestamp()" xlsl--ahf)
    ("llGetTime" "llGetTime()" xlsl--ahf)
    ("llGetTorque" "llGetTorque()" xlsl--ahf)
    ("llGetUnixTime" "llGetUnixTime()" xlsl--ahf)
    ("llGetUsedMemory" "llGetUsedMemory()" xlsl--ahf)
    ("llGetUsername" "llGetUsername( key id )" xlsl--ahf)
    ("llGetVel" "llGetVel()" xlsl--ahf)
    ("llGetWallclock" "llGetWallclock()" xlsl--ahf)
    ("llGiveInventoryList" "llGiveInventoryList(key destination, string category, list inventory)" xlsl--ahf)
    ("llGiveInventory" "llGiveInventory(key destination, string inventory)" xlsl--ahf)
    ("llGiveMoney" "llGiveMoney(key destination, integer amount)" xlsl--ahf)
    ("llGroundContour" "llGroundContour(vector offset)" xlsl--ahf)
    ("llGroundNormal" "llGroundNormal(vector offset)" xlsl--ahf)
    ("llGroundRepel" "llGroundRepel(float height, integer water, float tau)" xlsl--ahf)
    ("llGroundSlope" "llGroundSlope(vector offset)" xlsl--ahf)
    ("llGround" "llGround(vector offset)" xlsl--ahf)
    ("llHTTPRequest" "llHTTPRequest(string url, list parameters, string body)" xlsl--ahf)
    ("llHTTPResponse" "llHTTPResponse(key request_id, integer status, string body)" xlsl--ahf)
    ("llInsertString" "llInsertString(string dst, integer position, string src)" xlsl--ahf)
    ("llInstantMessage" "llInstantMessage(key user, string message)" xlsl--ahf)
    ("llIntegerToBase64" "llIntegerToBase64(integer number)" xlsl--ahf)
    ("llKey2Name" "llKey2Name(key id)" xlsl--ahf)
    ("llLinkParticleSystem" "llLinkParticleSystem( integer link, list rules )" xlsl--ahf)
    ("llLinks" "llLinks()" xlsl--ahf)
    ("llList2CSV" "llList2CSV(list src)" xlsl--ahf)
    ("llList2Float" "llList2Float(list src, integer index)" xlsl--ahf)
    ("llList2Integer" "llList2Integer(list src, integer index)" xlsl--ahf)
    ("llList2Key" "llList2Key(list src, integer index)" xlsl--ahf)
    ("llList2ListStrided" "llList2ListStrided(list src, integer start, integer end, integer stride)" xlsl--ahf)
    ("llList2List" "llList2List(list src, integer start, integer end)" xlsl--ahf)
    ("llList2Rot" "llList2Rot(list src, integer index)" xlsl--ahf)
    ("llList2String" "llList2String(list src, integer index)" xlsl--ahf)
    ("llList2Vector" "llList2Vector(list src, integer index)" xlsl--ahf)
    ("llListenControl" "llListenControl(integer number, integer active)" xlsl--ahf)
    ("llListenRemove" "llListenRemove(integer number)" xlsl--ahf)
    ("llListen" "llListen(integer channel, string name, key id, string msg)" xlsl--ahf)
    ("llListFindList" "llListFindList(list src, list test)" xlsl--ahf)
    ("llListInsertList" "llListInsertList(list dest, list src, integer pos)" xlsl--ahf)
    ("llListRandomize" "llListRandomize(list src, integer stride)" xlsl--ahf)
    ("llListReplaceList" "llListReplaceList(list dest, list src, integer start, integer end)" xlsl--ahf)
    ("llListSort" "llListSort(list src, integer stride, integer ascending)" xlsl--ahf)
    ("llListStatistics" "llListStatistics(integer operation, list input)" xlsl--ahf)
    ("llLoadURL" "llLoadURL(key avatar_id, string message, string url)" xlsl--ahf)
    ("llLog10" "llLog10(float val)" xlsl--ahf)
    ("llLog" "llLog(float val)" xlsl--ahf)
    ("llLookAt" "llLookAt(vector target, F32 strength, F32 damping)" xlsl--ahf)
    ("llLoopSoundMaster" "llLoopSoundMaster(string sound, float volume)" xlsl--ahf)
    ("llLoopSoundSlave" "llLoopSoundSlave()" xlsl--ahf)
    ("llLoopSound" "llLoopSound(string sound, float volume)" xlsl--ahf)
    ("llMapDestination" "llMapDestination(string simname, vector pos, vector look_at)" xlsl--ahf)
    ("llMD5String" "llMD5String(string src, integer nonce)" xlsl--ahf)
    ("llMessageLinked" "llMessageLinked(integer linknum, integer num, string str, key id)" xlsl--ahf)
    ("llMinEventDelay" "llMinEventDelay(float delay)" xlsl--ahf)
    ("llModifyLand" "llModifyLand(integer action, integer size)" xlsl--ahf)
    ("llModPow" "llModPow(integer a, integer b, integer c)" xlsl--ahf)
    ("llMoveToTarget" "llMoveToTarget(vector target, float tau)" xlsl--ahf)
    ("llOffsetTexture" "llOffsetTexture(float horizontal, float vertical, integer side)" xlsl--ahf)
    ("llOpenRemoteDataChannel" "llOpenRemoteDataChannel()" xlsl--ahf)
    ("llOverMyLand" "llOverMyLand(key id)" xlsl--ahf)
    ("llOwnerSay" "llOwnerSay(string message)" xlsl--ahf)
    ("llParcelMediaCommandList" "llParcelMediaCommandList(list commandList)" xlsl--ahf)
    ("llParcelMediaQuery" "llParcelMediaQuery(list query)" xlsl--ahf)
    ("llParseString2List" "llParseString2List(string src, list separators, list spacers)" xlsl--ahf)
    ("llParseStringKeepNulls" "llParseStringKeepNulls(string src, list separators, list spacers)" xlsl--ahf)
    ("llParticleSystem" "llParticleSystem(list parameters)" xlsl--ahf)
    ("llPassCollisions" "llPassCollisions(TRUE)" xlsl--ahf)
    ("llPassTouches" "llPassTouches(TRUE)" xlsl--ahf)
    ("llPlaySoundSlave" "llPlaySoundSlave(string sound, float volume)" xlsl--ahf)
    ("llPlaySound" "llPlaySound(string sound, float volume)" xlsl--ahf)
    ("llPointAt" "llPointAt(vector pos)" xlsl--ahf)
    ("llPow" "llPow(float base, float exp)" xlsl--ahf)
    ("llPreloadSound" "llPreloadSound(string sound)" xlsl--ahf)
    ("llPushObject" "llPushObject(key target, vector impulse, vector ang_impulse, integer local)" xlsl--ahf)
    ("llRefreshPrimURL" "llRefreshPrimURL()" xlsl--ahf)
    ("llRegionSayTo" "llRegionSayTo( key target, integer channel, string msg )" xlsl--ahf)
    ("llRegionSay" "llRegionSay(integer channel, string msg)" xlsl--ahf)
    ("llReleaseCamera" "llReleaseCamera(key agent)" xlsl--ahf)
    ("llReleaseControls" "llReleaseControls()" xlsl--ahf)
    ("llReleaseURL" "llReleaseURL(string url)" xlsl--ahf)
    ("llRemoteDataReply" "llRemoteDataReply(key channel, key message_id, string sdata, integer idata)" xlsl--ahf)
    ("llRemoteDataSetRegion" "llRemoteDataSetRegion()" xlsl--ahf)
    ("llRemoteLoadScriptPin" "llRemoteLoadScriptPin(key target, string name, integer pin, integer running, integer start_param)" xlsl--ahf)
    ("llRemoveFromLandBanList" "llRemoveFromLandBanList(key avatar)" xlsl--ahf)
    ("llRemoveFromLandPassList" "llRemoveFromLandPassList(key avatar)" xlsl--ahf)
    ("llRemoveInventory" "llRemoveInventory(string inventory)" xlsl--ahf)
    ("llRemoveVehicleFlags" "llRemoveVehicleFlags(integer flags)" xlsl--ahf)
    ("llRequestAgentData" "llRequestAgentData(key id, integer data)" xlsl--ahf)
    ("llRequestDisplayName" "llRequestDisplayName( key id )" xlsl--ahf)
    ("llRequestInventoryData" "llRequestInventoryData(string name)" xlsl--ahf)
    ("llRequestPermissions" "llRequestPermissions(key agent, integer perm)" xlsl--ahf)
    ("llRequestSecureURL" "llRequestSecureURL()" xlsl--ahf)
    ("llRequestSimulatorData" "llRequestSimulatorData(string simulator, integer data)" xlsl--ahf)
    ("llRequestURL" "llRequestURL()" xlsl--ahf)
    ("llRequestUsername" "llRequestUsername( key id )" xlsl--ahf)
    ("llResetLandBanList" "llResetLandBanList()" xlsl--ahf)
    ("llResetLandPassList" "llResetLandPassList()" xlsl--ahf)
    ("llResetOtherScript" "llResetOtherScript(string name)" xlsl--ahf)
    ("llResetScript" "llResetScript()" xlsl--ahf)
    ("llResetTime" "llResetTime()" xlsl--ahf)
    ("llRezAtRoot" "llRezAtRoot(string inventory, vector pos, vector vel, rotation rot, integer param)" xlsl--ahf)
    ("llRezObject" "llRezObject(string inventory, vector pos, vector vel, rotation rot, integer param)" xlsl--ahf)
    ("llRot2Angle" "llRot2Angle(rotation rot)" xlsl--ahf)
    ("llRot2Axis" "llRot2Axis(rotation rot)" xlsl--ahf)
    ("llRot2Euler" "llRot2Euler(rotation rot)" xlsl--ahf)
    ("llRot2Fwd" "llRot2Fwd(rotation q)" xlsl--ahf)
    ("llRot2Left" "llRot2Left(rotation rot)" xlsl--ahf)
    ("llRot2Up" "llRot2Up(rotation rot)" xlsl--ahf)
    ("llRotateTexture" "llRotateTexture(float angle, integer side)" xlsl--ahf)
    ("llRotBetween" "llRotBetween(vector a, vector b)" xlsl--ahf)
    ("llRotLookAt" "llRotLookAt(rotation target, float strength, float damping)" xlsl--ahf)
    ("llRotTargetRemove" "llRotTargetRemove(integer number)" xlsl--ahf)
    ("llRotTarget" "llRotTarget(rotation rot, float error)" xlsl--ahf)
    ("llRound" "llRound(float value)" xlsl--ahf)
    ("llSameGroup" "llSameGroup(key id)" xlsl--ahf)
    ("llSay" "llSay(integer channel, string text)" xlsl--ahf)
    ("llScaleTexture" "llScaleTexture()" xlsl--ahf)
    ("llScriptDanger" "llScriptDanger(vector pos)" xlsl--ahf)
    ("llScriptProfiler" "llScriptProfiler( integer flags )" xlsl--ahf)
    ("llSendRemoteData" "llSendRemoteData(key channel, string dest, integer idata, string sdata)" xlsl--ahf)
    ("llSensorRemove" "llSensorRemove()" xlsl--ahf)
    ("llSensorRepeat" "llSensorRepeat(string name, key id, integer type, float range, float arc, float rate)" xlsl--ahf)
    ("llSensor" "llSensor(string name, key id, integer type, float range, float arc)" xlsl--ahf)
    ("llSetAlpha" "llSetAlpha(float alpha, integer face)" xlsl--ahf)
    ("llSetBuoyancy" "llSetBuoyancy(float buoyancy)" xlsl--ahf)
    ("llSetCameraAtOffset" "llSetCameraAtOffset(vector offset)" xlsl--ahf)
    ("llSetCameraEyeOffset" "llSetCameraEyeOffset(vector offset)" xlsl--ahf)
    ("llSetCameraParams" "llSetCameraParams(list rules)" xlsl--ahf)
    ("llSetClickAction" "llSetClickAction(integer action)" xlsl--ahf)
    ("llSetColor" "llSetColor(vector color, integer face)" xlsl--ahf)
    ("llSetDamage" "llSetDamage(float damage)" xlsl--ahf)
    ("llSetForceAndTorque" "llSetForceAndTorque(vector force, vector torque, integer local)" xlsl--ahf)
    ("llSetForce" "llSetForce(vector force, integer local)" xlsl--ahf)
    ("llSetHoverHeight" "llSetHoverHeight(float height, integer water, float tau)" xlsl--ahf)
    ("llSetLinkAlpha" "llSetLinkAlpha(integer linknumber, float alpha, integer face)" xlsl--ahf)
    ("llSetLinkColor" "llSetLinkColor(integer linknumber, vector color, integer face)" xlsl--ahf)
    ("llSetLinkPrimitiveParamsFast" "llSetLinkPrimitiveParamsFast( integer linknumber, list rules )" xlsl--ahf)
    ("llSetLinkPrimitiveParams" "llSetLinkPrimitiveParams(integer linknumber, list rules)" xlsl--ahf)
    ("llSetLinkTextureAnim" "llSetLinkTextureAnim( integer link, integer mode, integer face, integer sizex, integer sizey, float start, float length, float rate )" xlsl--ahf)
    ("llSetLinkTexture" "llSetLinkTexture(integer linknumber, string texture, integer face)" xlsl--ahf)
    ("llSetLocalRot" "llSetLocalRot(rotation rot)" xlsl--ahf)
    ("llSetObjectDesc" "llSetObjectDesc(string name)" xlsl--ahf)
    ("llSetObjectName" "llSetObjectName(string name)" xlsl--ahf)
    ("llSetParcelMusicURL" "llSetParcelMusicURL(string url)" xlsl--ahf)
    ("llSetPayPrice" "llSetPayPrice(integer price, list quick_pay_buttons)" xlsl--ahf)
    ("llSetPos" "llSetPos(vector pos)" xlsl--ahf)
    ("llSetPrimitiveParams" "llSetPrimitiveParams(list rule)" xlsl--ahf)
    ("llSetPrimMediaParams" "llSetPrimMediaParams( integer face, list params )" xlsl--ahf)
    ("llSetPrimURL" "llSetPrimURL(string url)" xlsl--ahf)
    ("llSetRemoteScriptAccessPin" "llSetRemoteScriptAccessPin()" xlsl--ahf)
    ("llSetRot" "llSetRot(rotation rot)" xlsl--ahf)
    ("llSetScale" "llSetScale(vector scale)" xlsl--ahf)
    ("llSetScriptState" "llSetScriptState(string name, integer run)" xlsl--ahf)
    ("llSetSitText" "llSetSitText(string text)" xlsl--ahf)
    ("llSetSoundQueueing" "llSetSoundQueueing(integer queue)" xlsl--ahf)
    ("llSetSoundRadius" "llSetSoundRadius(float radius)" xlsl--ahf)
    ("llSetStatus" "llSetStatus(integer status, integer value)" xlsl--ahf)
    ("llSetTextureAnim" "llSetTextureAnim(integer mode, integer face, integer sizex, integer sizey, float start, float length, float rate)" xlsl--ahf)
    ("llSetTexture" "llSetTexture(string texture, integer side)" xlsl--ahf)
    ("llSetText" "llSetText(string text, vector color, float alpha)" xlsl--ahf)
    ("llSetTimerEvent" "llSetTimerEvent(float sec)" xlsl--ahf)
    ("llSetTorque" "llSetTorque(vector torque, integer local)" xlsl--ahf)
    ("llSetTouchText" "llSetTouchText(string text)" xlsl--ahf)
    ("llSetVehicleFlags" "llSetVehicleFlags(integer flag)" xlsl--ahf)
    ("llSetVehicleFloatParam" "llSetVehicleFloatParam(integer param, float value)" xlsl--ahf)
    ("llSetVehicleRotationParam" "llSetVehicleRotationParam(integer param, rotation rot)" xlsl--ahf)
    ("llSetVehicleType" "llSetVehicleType(integer type)" xlsl--ahf)
    ("llSetVehicleVectorParam" "llSetVehicleVectorParam(integer param, vector vec)" xlsl--ahf)
    ("llSHA1String" "llSHA1String(string src)" xlsl--ahf)
    ("llShout" "llShout(integer channel, string text)" xlsl--ahf)
    ("llSin" "llSin(float theta)" xlsl--ahf)
    ("llSitTarget" "llSitTarget(vector offset, rotation rot)" xlsl--ahf)
    ("llSleep" "llSleep(float sec)" xlsl--ahf)
    ("llSqrt" "llSqrt(float val)" xlsl--ahf)
    ("llStartAnimation" "llStartAnimation(string anim)" xlsl--ahf)
    ("llStopAnimation" "llStopAnimation(string anim)" xlsl--ahf)
    ("llStopHover" "llStopHover()" xlsl--ahf)
    ("llStopLookAt" "llStopLookAt()" xlsl--ahf)
    ("llStopMoveToTarget" "llStopMoveToTarget()" xlsl--ahf)
    ("llStopPointAt" "llStopPointAt()" xlsl--ahf)
    ("llStopSound" "llStopSound()" xlsl--ahf)
    ("llStringLength" "llStringLength(string src)" xlsl--ahf)
    ("llStringToBase64" "llStringToBase64(string str)" xlsl--ahf)
    ("llStringTrim" "llStringTrim(string src, integer type)" xlsl--ahf)
    ("llSubStringIndex" "llSubStringIndex(string source, string pattern)" xlsl--ahf)
    ("llTakeControls" "llTakeControls(integer controls, integer accept, integer pass_on)" xlsl--ahf)
    ("llTan" "llTan(float theta)" xlsl--ahf)
    ("llTargetOmega" "llTargetOmega(vector axis, float spinrate, float gain)" xlsl--ahf)
    ("llTargetRemove" "llTargetRemove(integer tnumber)" xlsl--ahf)
    ("llTarget" "llTarget(vector position, float range)" xlsl--ahf)
    ("llTeleportAgentHome" "llTeleportAgentHome(key id)" xlsl--ahf)
    ("llTextBox" "llTextBox( key avatar, string message, integer chat_channel )" xlsl--ahf)
    ("llToLower" "llToLower(string src)" xlsl--ahf)
    ("llToUpper" "llToUpper(string src)" xlsl--ahf)
    ("llTriggerSoundLimited" "llTriggerSoundLimited(string sound, float volume, vector tne, vector bsw)" xlsl--ahf)
    ("llTriggerSound" "llTriggerSound(key sound, float volume)" xlsl--ahf)
    ("llUnescapeURL" "llUnescapeURL(string url)" xlsl--ahf)
    ("llUnSit" "llUnSit(key id)" xlsl--ahf)
    ("llVecDist" "llVecDist(vector v1, vector v2)" xlsl--ahf)
    ("llVecMag" "llVecMag(vector v)" xlsl--ahf)
    ("llVecNorm" "llVecNorm(vector v)" xlsl--ahf)
    ("llVolumeDetect" "llVolumeDetect(integer detect)" xlsl--ahf)
    ("llWater" "llWater(vector offset)" xlsl--ahf)
    ("llWhisper" "llWhisper(integer channel, string text)" xlsl--ahf)
    ("llWind" "llWind(vector offset)" xlsl--ahf)
    ("llXorBase64StringsCorrect" "llXorBase64StringsCorrect(string s1, string s2)" xlsl--ahf)
    ("money" "money(key id, integer amount)
{
▮
}" xlsl--ahf)
    ("moving_end" "moving_end()
{
▮
}" xlsl--ahf)
    ("moving_start" "moving_start()
{
▮
}" xlsl--ahf)
    ("no_sensor" "no_sensor()
{
▮
}" xlsl--ahf)
    ("not_at_rot_target" "not_at_rot_target()
{
▮
}" xlsl--ahf)
    ("not_at_target" "not_at_target()
{
▮
}" xlsl--ahf)
    ("object_rez" "object_rez(key id)
{
▮
}" xlsl--ahf)
    ("on_rez" "on_rez(integer start_param)
{
▮
}" xlsl--ahf)
    ("remote_data" "remote_data(integer event_type, key channel, key message_id, string sender, integer idata, string sdata)
{
▮
}" xlsl--ahf)
    ("run_time_permissions" "run_time_permissions(integer perm)
{
▮
}" xlsl--ahf)
    ("sensor" "sensor(integer num_detected)
{
▮
}" xlsl--ahf)
    ("state_entry" "state_entry()
{
▮
}" xlsl--ahf)
    ("state_exit" "state_exit()
{
▮
}" xlsl--ahf)
    ("timer" "timer()
{
▮
}" xlsl--ahf)
    ("touch_end" "touch_end(integer num_detected)
{
▮
}" xlsl--ahf)
    ("touch_start" "touch_start(integer num_detected)
{
▮
}" xlsl--ahf)
    ("touch" "touch(integer num_detected)
{
▮
}" xlsl--ahf)
    ("while" "while (aa == bb)
{
▮
}" xlsl--ahf)

    )

  "abbrev table for `xlsl-mode'"
  )



(abbrev-table-put xlsl-abbrev-table :regexp "\\([_-0-9A-Za-z]+\\)")
(abbrev-table-put xlsl-abbrev-table :case-fixed t)
(abbrev-table-put xlsl-abbrev-table :system t)
(abbrev-table-put xlsl-abbrev-table :enable-function 'xlsl-abbrev-enable-function)




(defun xlsl-mode ()
  ;; (define-derived-mode xlsl-mode c-mode "LSL"
  "Major mode for editing LSL (Linden Scripting Language).

Shortcuts             Command Name
\\[comment-dwim]       `comment-dwim'

\\[xlsl-complete-symbol]      `xlsl-complete-symbol'

\\[xlsl-lookup-lsl-ref]     `xlsl-lookup-lsl-ref'

\\[xlsl-lookup-lsl-ref2]    `xlsl-lookup-lsl-ref2'

\\[xlsl-syntax-check]    `xlsl-syntax-check'

\\[xlsl-convert-rgb]     `xlsl-convert-rgb'

\\[xlsl-color-vectors-region]     `xlsl-color-vectors-region'

\\[xlsl-copy-all]          `xlsl-copy-all'

Complete documentation at URL `http://xahsl.org/sl/ls-emacs.html'."
  (interactive)
  (kill-all-local-variables)

  (cond
   ((= xlsl-mode-format-style 0) nil)
   ((= xlsl-mode-format-style 1) (progn
                                   ;; borrow c's indentation
                                   (c-mode)
                                   (setq indent-tabs-mode nil)
                                   (setq c-basic-offset 4)
                                   (c-set-offset 'substatement-open 0)
                                   (c-set-offset 'block-open 0)
                                   (c-set-offset 'statement-cont 0)))
   ((= xlsl-mode-format-style 2) (c-mode)))

  (setq major-mode 'xlsl-mode)
  (setq mode-name "LSL")
  (set-syntax-table xlsl-mode-syntax-table)
  (use-local-map xlsl-mode-map)

  (setq local-abbrev-table xlsl-abbrev-table)

  (make-local-variable 'abbrev-expand-function)
  (if (or
       (and (>= emacs-major-version 24)
            (>= emacs-minor-version 4))
       (>= emacs-major-version 25))
      (progn
        (setq abbrev-expand-function 'xlsl-expand-abbrev))
    (progn (add-hook 'abbrev-expand-functions 'xlsl-expand-abbrev nil t)))


  (setq font-lock-defaults '((xlsl-font-lock-keywords)))
  (run-mode-hooks 'xlsl-mode-hook))

(add-to-list 'auto-mode-alist '("\\.lsl\\'" . xlsl-mode))
(defun company-xlsl-backend (command &optional arg &rest ignored)
  (interactive (list 'interactive))

  (case command
    (interactive (company-begin-backend 'company-xlsl-backend))
    (prefix (and (eq major-mode 'xlsl-mode)
                 (company-grab-symbol)))
    (candidates)
    (remove-if-not
     (lambda (c) (string-prefix-p arg c))
     xlsl-all-keywords)))

(add-to-list 'company-backends 'company-xlsl-backend)
(provide 'xlsl-mode)

;;; xlsl-mode.el ends here
